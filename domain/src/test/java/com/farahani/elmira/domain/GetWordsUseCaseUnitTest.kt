package com.farahani.elmira.domain

import com.farahani.elmira.common.ErrorThrowable
import com.farahani.elmira.common.TestUtil
import com.farahani.elmira.common.mock
import com.farahani.elmira.common.transformer.TestCTransformer
import com.farahani.elmira.domain.repository.WordsRepository
import com.farahani.elmira.domain.usecase.common.invoke
import com.farahani.elmira.domain.usecase.venues.GetWordsUseCase
import io.reactivex.Completable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`


@RunWith(JUnit4::class)
class GetWordsUseCaseUnitTest {

    private val repository: WordsRepository = mock()
    private val getWordsUseCase = GetWordsUseCase(repository, TestCTransformer())

    @Test
    fun execute_onSuccess() {
        `when`(repository.getWords()).thenReturn(Completable.complete())

        getWordsUseCase.invoke()
            .test()
            .assertComplete()
    }

    @Test
    fun execute_onException() {
        `when`(repository.getWords()).thenReturn(Completable.error(TestUtil.error()))

        getWordsUseCase.invoke()
            .test()
            .assertError { it is ErrorThrowable }
            .assertNotComplete()
    }
}