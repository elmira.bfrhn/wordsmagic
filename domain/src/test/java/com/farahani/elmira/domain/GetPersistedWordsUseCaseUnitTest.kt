package com.farahani.elmira.domain

import com.farahani.elmira.common.ErrorThrowable
import com.farahani.elmira.common.TestUtil
import com.farahani.elmira.common.transformer.TestFTransformer
import com.farahani.elmira.domain.repository.WordsRepository
import com.farahani.elmira.domain.usecase.common.invoke
import com.farahani.elmira.domain.usecase.venues.GetPersistedWordsUseCase
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Flowable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

/**
 * Created by elmira on 17, April, 2020
 */

@RunWith(JUnit4::class)
class GetPersistedWordsUseCaseUnitTest {

    private val repository: WordsRepository = mock()
    private val useCase: GetPersistedWordsUseCase =
        GetPersistedWordsUseCase(repository, TestFTransformer())

    @Test
    fun execute_onSuccess() {
        Mockito.`when`(repository.getPersistedWords())
            .thenReturn(Flowable.just(TestUtil.wordObjectMap()))

        useCase.invoke()
            .test()
            .assertValue(TestUtil.wordObjectMap())
            .assertComplete()

        Mockito.verify(repository).getPersistedWords()
    }

    @Test
    fun execute_onException() {
        Mockito.`when`(repository.getPersistedWords()).thenReturn(Flowable.error(TestUtil.error()))

        useCase.invoke()
            .test()
            .assertError { it is ErrorThrowable }
            .assertNotComplete()
    }

}