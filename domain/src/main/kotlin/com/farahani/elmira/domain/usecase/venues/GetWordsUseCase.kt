package com.farahani.elmira.domain.usecase.venues

import com.farahani.elmira.domain.repository.WordsRepository
import com.farahani.elmira.domain.transformer.CTransformer
import com.farahani.elmira.domain.usecase.common.UseCaseCompletable
import io.reactivex.Completable
import javax.inject.Inject
class GetWordsUseCase @Inject constructor(
    private val repository: WordsRepository,
    private val transformer: CTransformer
) : UseCaseCompletable<Unit>() {
    override fun execute(param: Unit): Completable =
        repository.getWords().compose(transformer)
}