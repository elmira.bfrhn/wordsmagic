package com.farahani.elmira.domain.repository

import io.reactivex.Completable
import io.reactivex.Flowable
interface WordsRepository {

    fun getWords(): Completable

    fun getPersistedWords(): Flowable<HashMap<String, String>>
}