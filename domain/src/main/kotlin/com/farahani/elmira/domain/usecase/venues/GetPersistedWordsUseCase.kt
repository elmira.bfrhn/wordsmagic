package com.farahani.elmira.domain.usecase.venues

import com.farahani.elmira.domain.repository.WordsRepository
import com.farahani.elmira.domain.transformer.FTransformer
import com.farahani.elmira.domain.usecase.common.UseCaseFlowable
import io.reactivex.Flowable
import javax.inject.Inject
class GetPersistedWordsUseCase @Inject constructor(
    private val repository: WordsRepository,
    private val transformer: FTransformer<HashMap<String, String>>
) : UseCaseFlowable<HashMap<String, String>, Unit>() {
    override fun execute(param: Unit): Flowable<HashMap<String, String>> =
        repository.getPersistedWords().compose(transformer)
}