package com.farahani.elmira.domain.entity

import android.util.Log


data class WordObject(
    val id : Int,
    val textEng: String,
    val textSpa: String
) : DomainObject
