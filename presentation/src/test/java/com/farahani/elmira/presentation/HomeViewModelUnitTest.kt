package com.farahani.elmira.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.farahani.elmira.common.LiveDataUtil
import com.farahani.elmira.common.TestUtil
import com.farahani.elmira.common.mock
import com.farahani.elmira.domain.usecase.common.invoke
import com.farahani.elmira.domain.usecase.venues.GetPersistedWordsUseCase
import com.farahani.elmira.domain.usecase.venues.GetWordsUseCase
import com.farahani.elmira.presentation.features.home.viewmodel.HomeViewModel
import io.reactivex.Completable
import io.reactivex.Flowable
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doReturn

/**
 * Created by elmira on 17, April, 2020
 */
@RunWith(JUnit4::class)
class HomeViewModelUnitTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val getWordsUseCase: GetWordsUseCase = mock()
    private val getPersistedWordsUseCase: GetPersistedWordsUseCase = mock()


    private lateinit var viewModel: HomeViewModel

//    @Test
//    fun `on set words`() {
//        dataExistForGetWordsUseCase()
//        dataExistForGetPersistedWordsUseCase()
//        createViewModel()
//
//
//        assertEquals(
//            TestUtil.wordObjects(),
//            LiveDataUtil.getValue(viewModel.words)
//        )
//    }

    private fun dataDoesntExistForGetPersistedWords() {
        `when`(getPersistedWordsUseCase.invoke()).thenReturn(
            Flowable.error(
                TestUtil.error()
            )
        )
    }

    private fun dataDoesNotExistForGetWordsUseCase() {
        `when`(getWordsUseCase.invoke()).thenReturn(
            Completable.error(
                TestUtil.error()
            )
        )
    }

    private fun dataExistForGetPersistedWordsUseCase() {
        `when`(getPersistedWordsUseCase.invoke())
            .thenReturn(Flowable.just(TestUtil.wordObjectMap()))
    }

    private fun dataExistForGetWordsUseCase() =
        doReturn(Completable.complete()).`when`(getWordsUseCase).invoke()


    private fun createViewModel() {
        viewModel = HomeViewModel(
            getWordsUseCase,
            getPersistedWordsUseCase
        )
    }
}