package com.farahani.elmira.presentation.features.datails.viewmodel

import androidx.lifecycle.ViewModel
import com.farahani.elmira.presentation.common.di.PerFragment
import com.farahani.elmira.presentation.common.di.ViewModelKey
import com.farahani.elmira.presentation.features.result.viewmodel.ResultsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ResultsViewModelModule {
    @Binds
    @IntoMap
    @PerFragment
    @ViewModelKey(ResultsViewModel::class)
    abstract fun viewModel(viewModel: ResultsViewModel): ViewModel
}
