package com.farahani.elmira.presentation.common.executor

import com.farahani.elmira.domain.transformer.PostExecutionThread
import dagger.Binds
import dagger.Module
@Module
abstract class ExecutionModule {

    @Binds
    abstract fun postExecutionThread(uiThread: UiThread): PostExecutionThread

}