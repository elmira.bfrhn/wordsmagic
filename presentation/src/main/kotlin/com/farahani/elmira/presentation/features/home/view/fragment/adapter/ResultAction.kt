package com.farahani.elmira.presentation.features.home.view.fragment.adapter

import com.farahani.elmira.presentation.common.adapter.BaseAction
import com.farahani.elmira.presentation.model.UserPoint
data class ResultAction(val result: UserPoint) : BaseAction