package com.farahani.elmira.presentation.features.home.view.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.farahani.elmira.presentation.common.di.PerActivity
import com.farahani.elmira.presentation.common.di.PerFragment
import com.farahani.elmira.presentation.common.view.activity.BaseActivityModule
import com.farahani.elmira.presentation.features.result.view.ResultsFragment
import com.farahani.elmira.presentation.features.datails.view.ResultsFragmentModule
import com.farahani.elmira.presentation.features.home.view.fragment.HomeFragment
import com.farahani.elmira.presentation.features.home.view.fragment.HomeFragmentModule
import com.farahani.elmira.presentation.features.home.viewmodel.HomeViewModelModule
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
@Module(
    includes = [
        BaseActivityModule::class,
        HomeViewModelModule::class
    ]
)
abstract class HomeActivityModule {

    @Binds
    @PerActivity
    abstract fun appCompatActivity(homeActivity: HomeActivity): AppCompatActivity

    @PerFragment
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    abstract fun homeFragmentInjector(): HomeFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [ResultsFragmentModule::class])
    abstract fun detailsFragmentInjector(): ResultsFragment

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideLifecycle(homeActivity: HomeActivity): Lifecycle =
            homeActivity.lifecycle
    }
}