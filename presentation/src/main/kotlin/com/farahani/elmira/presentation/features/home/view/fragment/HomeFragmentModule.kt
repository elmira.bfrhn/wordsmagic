package com.farahani.elmira.presentation.features.home.view.fragment

import androidx.fragment.app.Fragment
import com.farahani.elmira.presentation.common.di.PerFragment
import dagger.Binds
import dagger.Module
@Module
abstract class HomeFragmentModule {

    @Binds
    @PerFragment
    abstract fun fragment(homeFragment: HomeFragment): Fragment
}