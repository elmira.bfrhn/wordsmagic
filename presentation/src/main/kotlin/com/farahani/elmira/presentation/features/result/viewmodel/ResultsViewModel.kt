package com.farahani.elmira.presentation.features.result.viewmodel

import androidx.lifecycle.MutableLiveData
import com.farahani.elmira.presentation.common.viewmodel.BaseViewModel
import com.farahani.elmira.presentation.model.UserPoint
import javax.inject.Inject
class ResultsViewModel @Inject constructor(
) : BaseViewModel() {
    val result = MutableLiveData<UserPoint>()

    fun setData(it: UserPoint) {
        result.value = it
    }
}