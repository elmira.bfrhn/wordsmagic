package com.farahani.elmira.presentation

import com.farahani.elmira.presentation.common.CommonModule
import dagger.Module
@Module(includes = [CommonModule::class])
abstract class PresentationModule