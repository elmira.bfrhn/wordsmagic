package com.farahani.elmira.presentation.common.error

data class ErrorModel(val error: Error)