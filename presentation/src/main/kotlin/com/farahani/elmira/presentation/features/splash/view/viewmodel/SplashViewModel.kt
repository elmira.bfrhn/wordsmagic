package com.farahani.elmira.presentation.features.splash.view.viewmodel

import android.os.CountDownTimer
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.farahani.elmira.domain.usecase.common.invoke
import com.farahani.elmira.domain.usecase.venues.GetWordsUseCase
import com.farahani.elmira.presentation.common.extension.orFalse
import com.farahani.elmira.presentation.common.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * Created by elmira on 18, April, 2020
 */
class SplashViewModel @Inject constructor(
    private val getWordsUseCase: GetWordsUseCase
) : BaseViewModel() {

    private val isAnimationFinished = MutableLiveData<Boolean>(false)
    private val isWordsPersisted = MutableLiveData<Boolean>(false)

    private val animationTimer = object : CountDownTimer(8000, 1000) {
        override fun onTick(millisUntilFinished: Long) {}

        override fun onFinish() {
            isAnimationFinished.value = true
        }
    }

    val finnishSplashMediatorLiveData = MediatorLiveData<Boolean>().apply {
        addSource(isWordsPersisted) {
            this.value = it && isAnimationFinished.value.orFalse()
        }
        addSource(isAnimationFinished) {
            this.value = it && isWordsPersisted.value.orFalse()
        }
    }

    init {
        animationTimer.start()
        getWords()


    }

    private fun getWords() {
        getWordsUseCase()
            .onError()
            .subscribe({
                isWordsPersisted.value = true
            }, {

            }).track()
    }
}