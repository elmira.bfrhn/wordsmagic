package com.farahani.elmira.presentation.common.extension

/**
 * Created by elmira on 18, April, 2020
 */
fun Boolean?.orFalse(): Boolean = this ?: false