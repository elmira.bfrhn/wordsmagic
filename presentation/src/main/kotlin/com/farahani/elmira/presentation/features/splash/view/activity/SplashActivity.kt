package com.farahani.elmira.presentation.features.splash.view.activity

import android.content.Intent
import android.os.Bundle
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.extension.observe
import com.farahani.elmira.presentation.common.extension.viewModelProvider
import com.farahani.elmira.presentation.common.view.activity.BaseActivity
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory
import com.farahani.elmira.presentation.features.home.view.activity.HomeActivity
import com.farahani.elmira.presentation.features.splash.view.viewmodel.SplashViewModel
import javax.inject.Inject


class SplashActivity : BaseActivity() {
    private var isAnimationFinished: Boolean = false

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        viewModel = viewModelProvider(factory)

        observe(viewModel.finnishSplashMediatorLiveData, ::observeLoading)
    }

    private fun observeLoading(finishSplash: Boolean) {
        if (finishSplash) {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }
    }
}