package com.farahani.elmira.presentation.features.home.viewmodel

import android.os.CountDownTimer
import android.text.format.DateUtils
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.farahani.elmira.domain.usecase.common.invoke
import com.farahani.elmira.domain.usecase.venues.GetPersistedWordsUseCase
import com.farahani.elmira.presentation.common.adapter.BaseAction
import com.farahani.elmira.presentation.common.extension.observeOnce
import com.farahani.elmira.presentation.common.viewmodel.BaseViewModel
import com.farahani.elmira.presentation.features.home.view.fragment.adapter.ResultAction
import com.farahani.elmira.presentation.model.UserPoint
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val getPersistedWordsUseCase: GetPersistedWordsUseCase
) : BaseViewModel() {

    val clickObservable = MutableLiveData<BaseAction>()

    val words: LiveData<HashMap<String, String>> =
        LiveDataReactiveStreams.fromPublisher(getPersistedWordsUseCase.invoke())

    val loading = MutableLiveData<Boolean>(true)

    var currentWord = MutableLiveData<MutableMap.MutableEntry<String, String>>()
    var answerWord = MutableLiveData<MutableMap.MutableEntry<String, String>>()

    private var point: Int = 0
    private var wrongAnswerCount: Int = 0
    private var correctAnswerCount: Int = 0

    @VisibleForTesting
    var timer: CountDownTimer

    private val _currentTime = MutableLiveData<Long>()

    val currentTimeString = Transformations.map(_currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    init {

        timer = object : CountDownTimer(ANSWER_TIME, ONE_SECOND) {
            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = (millisUntilFinished / ONE_SECOND)
            }

            override fun onFinish() {
                selectNext()
                _currentTime.value = DONE
            }
        }

        timer.start()

        words.observeOnce {
            loading.value = false
            setNextWord()
            setAnswer()
        }
    }

    private fun setNextWord() {
        words.value?.let {
            if (it.isNotEmpty()) {
                currentWord.value = it.entries.elementAt(
                    (it.values.indices).random()
                )
            }
        }
    }

    private fun setAnswer() {
        val displayCorrectAnswerChance = (0..1000).random()
        words.value?.let {
            if (it.isNotEmpty()) {
                if (displayCorrectAnswerChance < 500) {
                    answerWord.value = it.entries.elementAt((it.values.indices).random())
                } else {
                    answerWord.value = currentWord.value
                }
            }
        }
    }

    fun checkSelectWrong() {
        if (currentWord.value == answerWord.value) {
            if (point > 0) {
                point = point.minus(10)
            }
            wrongAnswerCount++
        } else {
            point = point.plus(10)
            correctAnswerCount++
        }
        selectNext()
    }

    fun checkSelectCorrect() {
        if (currentWord.value == answerWord.value) {
            point = point.plus(10)
            correctAnswerCount++
            //user is correct
        } else {
            if (point > 0) {
                point = point.minus(10)
            }
            wrongAnswerCount++
            // user is wrong
        }
        selectNext()
    }

    fun selectNext() {
        words.value?.let {
            if (it.isNotEmpty()) {
                removeCurrentWordFromList()
                setNextWord()
                setAnswer()
                timer.start()
            } else {
                timer.cancel()
                updateResult(point, wrongAnswerCount, correctAnswerCount)
            }
        }
    }

    private fun removeCurrentWordFromList() {
        currentWord.value?.let { words.value?.remove(it.key) }
    }

    private fun updateResult(
        point: Int,
        wrongAnswerCount: Int,
        correctAnswerCount: Int
    ) {
        clickObservable.value =
            ResultAction(UserPoint(point, wrongAnswerCount, correctAnswerCount))
    }

    companion object {
        private const val DONE = 0L
        private const val ONE_SECOND = 1000L
        private const val ANSWER_TIME = 10000L
    }
}