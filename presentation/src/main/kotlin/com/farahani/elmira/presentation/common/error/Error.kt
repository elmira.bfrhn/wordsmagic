package com.farahani.elmira.presentation.common.error
data class Error(val errorCode: Int, val errorMessage: String?)