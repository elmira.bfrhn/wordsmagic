package com.farahani.elmira.presentation.features.home.view.activity

import android.os.Bundle
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.adapter.BaseAction
import com.farahani.elmira.presentation.common.extension.observe
import com.farahani.elmira.presentation.common.extension.viewModelProvider
import com.farahani.elmira.presentation.common.view.activity.BaseActivity
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory
import com.farahani.elmira.presentation.features.home.view.fragment.HomeFragment
import com.farahani.elmira.presentation.features.home.view.fragment.adapter.ResultAction
import com.farahani.elmira.presentation.features.home.viewmodel.HomeViewModel
import com.farahani.elmira.presentation.features.result.view.ResultsFragment
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        startGameTextView.setOnClickListener {
            if (savedInstanceState == null) {
                addFragment(R.id.fragmentContainer, HomeFragment.newInstance())
            }
        }
        viewModel = viewModelProvider(factory)

        observe(viewModel.clickObservable, ::observeClicks)
    }

    private fun observeClicks(action: BaseAction) {
        when (action) {
            is ResultAction -> {
                supportFragmentManager.popBackStackImmediate()
                addFragment(
                    R.id.fragmentContainer,
                    ResultsFragment.newInstance(action.result),
                    false
                )
            }
        }
    }
}