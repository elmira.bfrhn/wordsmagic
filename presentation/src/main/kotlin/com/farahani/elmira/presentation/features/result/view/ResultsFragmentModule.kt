package com.farahani.elmira.presentation.features.datails.view

import androidx.fragment.app.Fragment
import com.farahani.elmira.presentation.common.di.PerFragment
import com.farahani.elmira.presentation.features.datails.viewmodel.ResultsViewModelModule
import com.farahani.elmira.presentation.features.result.view.ResultsFragment
import dagger.Binds
import dagger.Module
@Module(includes = [ResultsViewModelModule::class])
abstract class ResultsFragmentModule {

    @Binds
    @PerFragment
    abstract fun fragment(resultsFragment: ResultsFragment): Fragment
}