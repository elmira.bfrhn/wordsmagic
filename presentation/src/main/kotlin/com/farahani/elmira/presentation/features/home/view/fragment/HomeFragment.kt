package com.farahani.elmira.presentation.features.home.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.extension.observe
import com.farahani.elmira.presentation.common.extension.viewModelProvider
import com.farahani.elmira.presentation.common.extension.visible
import com.farahani.elmira.presentation.common.view.fragment.BaseFragment
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory
import com.farahani.elmira.presentation.features.home.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)

        observe(viewModel.messageObservable, ::showMessage)
        observe(viewModel.loading, ::observeLoading)
        observe(viewModel.currentWord, ::observeCurrentWord)
        observe(viewModel.answerWord, ::observeAnswerWord)
        observe(viewModel.currentTimeString, ::observeTime)
    }

    private fun observeLoading(loading: Boolean) {
        progressBar.visible(loading)
        buttonNext.visible(!loading)
    }

    private fun observeTime(currentTime: String) {
        timer.text = currentTime
    }

    private fun observeCurrentWord(currentWord: MutableMap.MutableEntry<String, String>?) {
        currentWordTextView.text = currentWord?.value ?: ""
    }

    private fun observeAnswerWord(answerWord: MutableMap.MutableEntry<String, String>?) {
        answerWordTextView.text = answerWord?.key ?: ""
        resetAnimation()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        container.setOnClickListener {}
        buttonWrong.setOnClickListener {
            viewModel.checkSelectWrong()
            resetAnimation()
        }
        buttonCorrect.setOnClickListener {
            viewModel.checkSelectCorrect()
            resetAnimation()
        }
        buttonNext.setOnClickListener {
            viewModel.selectNext()
            resetAnimation()
        }
    }

    private fun resetAnimation() {
        container.progress = 0F
        container.transitionToEnd()
    }

    companion object {
        fun newInstance() = HomeFragment()
    }
}