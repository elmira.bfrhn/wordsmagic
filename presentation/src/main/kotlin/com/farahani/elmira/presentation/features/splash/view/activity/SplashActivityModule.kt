package com.farahani.elmira.presentation.features.splash.view.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.farahani.elmira.presentation.common.di.PerActivity
import com.farahani.elmira.presentation.common.view.activity.BaseActivityModule
import com.farahani.elmira.presentation.features.splash.view.viewmodel.SplashViewModelModule
import dagger.Binds
import dagger.Module
import dagger.Provides


@Module(
    includes = [
        BaseActivityModule::class,
        SplashViewModelModule::class
    ]
)
abstract class SplashActivityModule {

    @Binds
    @PerActivity
    abstract fun appCompatActivity(splashActivity: SplashActivity): AppCompatActivity

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideLifecycle(splashActivity: SplashActivity): Lifecycle =
            splashActivity.lifecycle
    }
}