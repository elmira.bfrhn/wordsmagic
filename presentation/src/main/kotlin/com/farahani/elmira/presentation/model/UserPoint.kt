package com.farahani.elmira.presentation.model

import java.io.Serializable


data class UserPoint(
    val point: Int,
    val wrongAnswerCount: Int,
    val correctAnswerCount: Int
) : Serializable