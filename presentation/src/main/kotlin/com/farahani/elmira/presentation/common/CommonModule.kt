package com.farahani.elmira.presentation.common

import com.farahani.elmira.presentation.common.executor.ExecutionModule
import dagger.Module
@Module(includes = [ExecutionModule::class])
abstract class CommonModule