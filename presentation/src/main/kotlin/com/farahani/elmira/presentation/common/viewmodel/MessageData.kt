package com.farahani.elmira.presentation.common.viewmodel

import androidx.annotation.StringRes
data class MessageData(
    var message: String? = null,
    @StringRes
    var resource: Int? = null
)
