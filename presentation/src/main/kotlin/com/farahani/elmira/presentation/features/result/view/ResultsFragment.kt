package com.farahani.elmira.presentation.features.result.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.farahani.elmira.presentation.R
import com.farahani.elmira.presentation.common.extension.observe
import com.farahani.elmira.presentation.common.extension.viewModelProvider
import com.farahani.elmira.presentation.common.view.fragment.BaseFragment
import com.farahani.elmira.presentation.common.viewmodel.ViewModelProviderFactory
import com.farahani.elmira.presentation.features.result.viewmodel.ResultsViewModel
import com.farahani.elmira.presentation.features.home.view.activity.HomeActivity
import com.farahani.elmira.presentation.model.UserPoint
import kotlinx.android.synthetic.main.fragment_result.*
import javax.inject.Inject

class ResultsFragment : BaseFragment() {

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private lateinit var viewModel: ResultsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)

        arguments?.getSerializable("result")?.let { viewModel.setData(it as UserPoint) }

        observe(viewModel.result, ::observeResult)
    }

    private fun observeResult(result: UserPoint) {
        point.text = resources.getString(R.string.point, result.point)
        wrongAnswers.text =
            resources.getString(R.string.wrong_answer_count, result.wrongAnswerCount)
        correctAnswers.text =
            resources.getString(R.string.correct_answer_count, result.correctAnswerCount)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_result, container, false)

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        container.setOnClickListener {}

        startNewGame.setOnClickListener {
            this.activity?.let {
                it.finish()
                startActivity(Intent(this.context, HomeActivity::class.java))
            }
        }
    }

    companion object {
        fun newInstance(result: UserPoint) = ResultsFragment().apply {
            arguments = Bundle().apply {
                putSerializable("result", result)
            }
        }
    }

}