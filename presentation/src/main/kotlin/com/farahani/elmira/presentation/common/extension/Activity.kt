package com.farahani.elmira.presentation.common.extension

import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
fun FragmentActivity.requestPermission(requestCode: Int, vararg permissions: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    }
}

fun FragmentActivity.checkAppPermission(permission: String): Boolean =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
        ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    else
        true