package com.farahani.elmira.data.datasource

import dagger.Binds
import dagger.Module
import javax.inject.Singleton


@Module
abstract class DataSourceModule {

    @Binds
    @Singleton
    abstract fun venuesDataSource(venuesDataSource: WordsDataSourceImpl): WordsDataSource
}