package com.farahani.elmira.data.dataservice

import dagger.Module
import dagger.Provides
import dagger.Reusable


@Module
class NetworkModule {

    @Provides
    @Reusable
    fun venuesDataService(dataServiceProvider: DataServiceFactory) =
        dataServiceProvider.create(WordsDataService::class.java)
}