package com.farahani.elmira.data.entity.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.farahani.elmira.data.entity.dao.WordsDao
import com.farahani.elmira.data.entity.local.WordEntity


@Database(
    entities = [
        WordEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class WordsDB : RoomDatabase() {

    abstract fun venuesDao(): WordsDao
}
