package com.farahani.elmira.data.entity.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farahani.elmira.data.entity.local.WordEntity
import io.reactivex.Flowable
@Dao
interface WordsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(words: List<WordEntity>)

    @Query("SELECT * FROM words")
    fun selectAll(): Flowable<List<WordEntity>>
}