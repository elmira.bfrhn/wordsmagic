package com.farahani.elmira.data.entity.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "words")
data class WordEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = "pair_id")
    val id: Int,
    @ColumnInfo(index = true, name = "text_eng")
    val textEng: String,
    @ColumnInfo(index = true, name = "text_spa")
    val textSpa: String
)