package com.farahani.elmira.data.entity.extensions

import com.farahani.elmira.data.entity.local.WordEntity
import com.farahani.elmira.domain.entity.WordObject
fun WordEntity.map() = WordObject(id,textEng, textSpa)