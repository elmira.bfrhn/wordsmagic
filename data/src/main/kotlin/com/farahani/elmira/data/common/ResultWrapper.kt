package com.farahani.elmira.data.common

import com.farahani.elmira.data.common.error.ErrorHandler
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Extension to get and handle errors
 */
fun <T> Flowable<T>.onError(): Flowable<T> =
    this.doOnError {
        // TODO Fire crashlytics event here
        throw it.asErrorResult()
    }

fun <T> Observable<T>.onError(): Observable<T> =
    this.doOnError {
        // TODO Fire crashlytics event here
        throw it.asErrorResult()
    }

fun <T> Single<T>.onError(): Single<T> =
    this.doOnError {
        // TODO Fire crashlytics event here
        throw it.asErrorResult()
    }

fun Completable.onError(): Completable =
    this.doOnError {
        // TODO Fire crashlytics event here
        throw it.asErrorResult()
    }


fun Throwable.asErrorResult(): Throwable = ErrorHandler.convert(this)