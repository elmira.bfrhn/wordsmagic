package com.farahani.elmira.data.dataservice

import com.farahani.elmira.data.BuildConfig
import com.farahani.elmira.data.entity.dto.Word
import io.reactivex.Flowable
import retrofit2.http.GET


interface WordsDataService {

    @GET(value = "${BuildConfig.BASE_URL}words.json")
    fun getWords(): Flowable<List<Word>>
}

