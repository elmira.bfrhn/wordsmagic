package com.farahani.elmira.data.datasource

import com.farahani.elmira.data.entity.local.WordEntity
import io.reactivex.Completable
import io.reactivex.Flowable


interface WordsDataSource {

    fun getWords(): Completable

    fun getPersistedWordPairs(): Flowable<List<WordEntity>>
}