package com.farahani.elmira.data.common.error

object ErrorCode {
    const val ERROR_HAPPENED = 1000
    const val ERROR_TIMEOUT = 1001
    const val ERROR_IO = 1002
    const val ERROR_EMPTY_STATE = 1003

    const val ERROR_HAPPENED_MESSAGE = "َAn error happened!"
    const val ERROR_TIMEOUT_MESSAGE = "Cannot connect try later."
    const val ERROR_IO_MESSAGE = "There is a problem in connection!"
}