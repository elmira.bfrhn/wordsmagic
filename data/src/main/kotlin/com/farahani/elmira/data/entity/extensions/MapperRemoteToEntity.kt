package com.farahani.elmira.data.entity.extensions

import com.farahani.elmira.data.entity.dto.Word
import com.farahani.elmira.data.entity.local.WordEntity
fun Word.map() = WordEntity(0,textEng, textSpa)