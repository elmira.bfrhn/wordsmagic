package com.farahani.elmira.data.repository

import com.farahani.elmira.data.datasource.WordsDataSource
import com.farahani.elmira.data.entity.extensions.map
import com.farahani.elmira.domain.entity.WordObject
import com.farahani.elmira.domain.repository.WordsRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject


class WordsRepositoryImpl @Inject constructor(
    private val dataSource: WordsDataSource
) : WordsRepository {

    override fun getWords(): Completable =
        dataSource.getWords()

    override fun getPersistedWords(): Flowable<HashMap<String, String>> {
        val hashMap = HashMap<String, String>()
        return dataSource.getPersistedWordPairs()
            .map {
                it.forEach {
                    hashMap[it.textSpa] = it.textEng
                }
                hashMap
            }
    }
}
