package com.farahani.elmira.data.common.error

class ErrorThrowable(val code: Int, message: String?) : Throwable(message) {

    constructor(code: Int) : this(code, null)

}