package com.farahani.elmira.data.executor

import com.farahani.elmira.domain.transformer.ThreadExecutor
import dagger.Binds
import dagger.Module

@Module
abstract class ExecutionModule {

    @Binds
    abstract fun threadExecutor(jobExecutor: JobExecutor): ThreadExecutor

}