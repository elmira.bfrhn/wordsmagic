package com.farahani.elmira.data.datasource

import com.farahani.elmira.data.common.onError
import com.farahani.elmira.data.dataservice.WordsDataService
import com.farahani.elmira.data.entity.dao.WordsDao
import com.farahani.elmira.data.entity.extensions.map
import com.farahani.elmira.data.entity.local.WordEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class WordsDataSourceImpl @Inject constructor(
    private val dataService: WordsDataService,
    private val wordsDao: WordsDao
) : WordsDataSource {

    override fun getWords(): Completable =
        persistData()

    override fun getPersistedWordPairs(): Flowable<List<WordEntity>> =
        wordsDao.selectAll().onError()

    private fun persistData(): Completable {
        return dataService.getWords().onError()
            .doOnNext {
                wordsDao.insert(it.map { it.map() })
            }.ignoreElements()
    }
}