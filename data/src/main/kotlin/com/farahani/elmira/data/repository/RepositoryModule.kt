package com.farahani.elmira.data.repository


import com.farahani.elmira.data.datasource.DataSourceModule
import com.farahani.elmira.domain.repository.WordsRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module(includes = [DataSourceModule::class])
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun venuesRepository(wordsRepository: WordsRepositoryImpl): WordsRepository
}