package com.farahani.elmira.data.common.error

data class Error(val errorCode: Int, val errorMessage: String?)