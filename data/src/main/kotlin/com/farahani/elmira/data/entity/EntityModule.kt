package com.farahani.elmira.data.entity

import android.app.Application
import androidx.room.Room
import com.farahani.elmira.data.entity.db.WordsDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class EntityModule {

    @Provides
    fun venuesDao(db: WordsDB) = db.venuesDao()

    @Provides
    @Singleton
    fun database(application: Application): WordsDB = Room.databaseBuilder(
        application.applicationContext,
        WordsDB::class.java,
        "places_db"
    ).fallbackToDestructiveMigration()
        .build()
}