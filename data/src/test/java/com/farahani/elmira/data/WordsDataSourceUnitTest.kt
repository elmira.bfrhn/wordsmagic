package com.farahani.elmira.data

import com.farahani.elmira.common.TestUtil
import com.farahani.elmira.common.mock
import com.farahani.elmira.data.dataservice.WordsDataService
import com.farahani.elmira.data.datasource.WordsDataSourceImpl
import com.farahani.elmira.data.entity.dao.WordsDao
import com.nhaarman.mockitokotlin2.spy
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

/**
 * Created by elmira on 17, April, 2020
 */
@RunWith(JUnit4::class)
class WordsDataSourceUnitTest {


    private val wordsDataService: WordsDataService = mock()
    private val wordsDao: WordsDao = mock()

    private lateinit var dataSource: WordsDataSourceImpl

    @Before
    fun setup() {
        dataSource = spy(
            WordsDataSourceImpl(
                wordsDataService,
                wordsDao
            )
        )
    }

    @Test
    fun `get words from api success`() {
        Mockito.`when`(wordsDataService.getWords()).thenReturn(Flowable.just(TestUtil.words()))

        dataSource.getWords()
            .test()
            .assertComplete()
    }

    @Test
    fun `get words from api fail`() {
        Mockito.`when`(wordsDataService.getWords())
            .thenReturn(Flowable.error(TestUtil.error()))

        dataSource.getWords()
            .test()
            .assertNotComplete()
    }

    @Test
    fun `get persisted words success`() {
        Mockito.`when`(wordsDao.selectAll()).thenReturn(Flowable.just(TestUtil.wordEntities()))

        dataSource.getPersistedWordPairs()
            .test()
            .assertValue(TestUtil.wordEntities())
            .assertComplete()
    }

    @Test
    fun `get persisted words fail`() {
        Mockito.`when`(wordsDao.selectAll())
            .thenReturn(Flowable.error(TestUtil.error()))

        dataSource.getPersistedWordPairs()
            .test()
            .assertNotComplete()
    }
}
