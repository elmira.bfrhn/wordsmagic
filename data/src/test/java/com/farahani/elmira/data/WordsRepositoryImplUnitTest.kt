package com.farahani.elmira.data

import com.farahani.elmira.common.ErrorThrowable
import com.farahani.elmira.common.TestUtil
import com.farahani.elmira.data.datasource.WordsDataSource
import com.farahani.elmira.data.repository.WordsRepositoryImpl
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Completable
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(JUnit4::class)
class WordsRepositoryImplUnitTest {

    private val dataSource: WordsDataSource = mock()
    private lateinit var repository: WordsRepositoryImpl


    @Before
    fun setup() {
        repository = WordsRepositoryImpl(dataSource)
    }

    @Test
    fun `get words from api success`() {
        `when`(
            dataSource.getWords()
        ).thenReturn(Completable.complete())

        repository.getWords()
            .test()
            .assertComplete()

        verify(dataSource).getWords()
    }

    @Test
    fun `get words from api fails`() {
        `when`(dataSource.getWords())
            .thenReturn(Completable.error(TestUtil.error()))

        repository.getWords()
            .test()
            .assertError { it is ErrorThrowable }
            .assertNotComplete()

        verify(dataSource).getWords()

    }

    @Test
    fun `get persisted words from db success`() {
        `when`(dataSource.getPersistedWordPairs())
            .thenReturn(Flowable.just(TestUtil.wordEntities()))

        repository.getPersistedWords()
            .test()
            .assertValue(TestUtil.wordObjects())
            .assertComplete()
    }

    @Test
    fun `get persisted words from db fails`() {
        `when`(
            dataSource.getPersistedWordPairs()
        ).thenReturn(
            Flowable.error(TestUtil.error())
        )
        repository.getPersistedWords()
            .test()
            .assertError { it is ErrorThrowable }
            .assertNotComplete()

        verify(dataSource).getPersistedWordPairs()

    }
}