>  GENERAL INFORMATION

This project implemented using :
* Kotlin, 
* RxJAva, 
* Dagger,
* Retrofit,
* okhttp,
* MotionLayout,
* LifeCycle,
* Room,
* LiveData and some other JetPack libraries 

  ****Implemented according to Uncle Bob's Clean Architecture!****

Presentation layer architecture is **MVVM**.

I have implemented this project using two Activities and two fragments:
- Splash Activity
- HomeActivity - > Home Fragment -> Result Fragment


I have added some tests for domain and data layer but I could not add presentation layer and Espresso tests because of lack of time.

This project works as below:

First page : Splash screen 

Show a nice animation for 8 seconds.

Second page : 

Show a button to start the game.

Third page: 

Game page in which we have 

* **a timer**: user can guess the word until this count down to 00:00
* **a word in the center of screen** : in first language here is English
* **a word floating in the air** : in second language here is spanish
* **a correct and a wrong button** : user can select them to say floating word is a correct translation of first word or not.
* and a **skip button at the bottom **of the page for user if they do not want to answer to the question.

Fourth page : Result 
After all words finished user automatically navigate to result page.
There are 3 texts in this page :

* Point : for each correct answer 10 points add to user's total points.
* Wrong answer count 
* and Correct answer count


There are also 2 buttons 
* Start New game : navigate user to new game page
* Save result : save result of this game  **(NOT IMPLEMENTED YET because of lack of time)**


****There are some rooms for improvement in this project for future****

* Add ui tests using Espresso
* Add settings and let user to choose which language to learn
* Add presentation (view models ) unit tests
* Add guide in first page for user to know how to play the game
* Add animations to result page

>    **ANSWER To QUESTIONS**

**1-how much time was invested (within the given limit)?**


I have started the task on the April 15th on 8:00 Pm 

And Finished it on the April 17th on 8:00 Pm

Each day I worked for 8 to 10 hours.


**2-how was the time distributed (concept, model layer, view(s), game mechanics)?**

**if we consider the whole time of 30:**
* Base Architecture: 8/30
* Data and domain layer:3/30
* GameLogic: 3/30
* Views Animations: 6/30
* Presentation layer Implementation: 7/30
* Tests:3/30

**3-decisions made to solve certain aspects of the game?**

**Three things to consider:**

1. First how to choose the first word: 
 I generate a random number from all words array index and select that word then remove it from the array.
 
2. Second how to generate the second word (the answer) : 
 I generate a random number between 0 to 1000 and decide if the random number 
is greater than 500 then I display the correct answer otherwise select a random word from the hashmap.
 
3. Third how to calculate the points: 
 I consider to add a 10 point for each correct answer and minus 10 point for each wrong answer.

**4-decisions made because of restricted time**
* did not handle screen rotation(Landscape mode)
* did not add UI tests 
* did not complete Presentation layer tests
* did not persist the result of the game in DB
* did not add a setting or guide to app

**5-what would be the first thing to improve or add if there had been more time?**
- Check and test thoroughly, to see if there is a bug 
- Add tests to the presentation layer
- Persist user result 
- Add animations to result page



