package com.farahani.elmira.common
class ErrorThrowable(val code: Int, message: String?) : Throwable(message) {

    constructor(code: Int) : this(code, null)

}