package com.farahani.elmira.common.transformer

import com.farahani.elmira.domain.transformer.CTransformer
import io.reactivex.Completable
import io.reactivex.CompletableSource

class TestCTransformer : CTransformer() {

    override fun apply(upstream: Completable): CompletableSource {
        return upstream
    }

}