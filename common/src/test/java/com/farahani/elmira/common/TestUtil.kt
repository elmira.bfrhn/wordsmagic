package com.farahani.elmira.common

import com.farahani.elmira.data.entity.dto.Word
import com.farahani.elmira.data.entity.extensions.map
import com.farahani.elmira.domain.entity.WordObject
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by elmira on 17, April, 2020
 */
object TestUtil {
    private val gson = Gson()

    fun wordObject() = WordObject(0, "holidays", "vacaciones")

    fun wordObjectMap() = hashMapOf(
        wordObject().textEng to wordObject().textSpa
    )

    fun error(): ErrorThrowable = ErrorThrowable(ErrorCode.ERROR_HAPPENED)

    fun words(): List<Word> {
        val listType = object : TypeToken<List<Word>>() {}.type
        return gson.fromJson(parseJson("words.json"), listType) as List<Word>
    }

    fun wordEntities() = words().map { it.map() }

    private fun parseJson(fileName: String): String =
        javaClass.classLoader?.getResourceAsStream("json/$fileName")
            ?.bufferedReader().use { it?.readText().orEmpty() }

    fun wordObjects(): HashMap<String, String> {
        val hashMap = HashMap<String, String>()
        wordEntities().forEach {
            hashMap[it.textSpa] = it.textEng
        }
        return hashMap
    }
}