package com.farahani.elmira.wordsmagic

import android.util.Log
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


class WordsMagicApp : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        createDBLogger()
    }


    private fun createDBLogger() {
        if (BuildConfig.DEBUG) {
            val debugDB = Class.forName("com.amitshekhar.DebugDB")
            val getAddressLog = debugDB.getMethod("getAddressLog")
            val value = getAddressLog.invoke(null)
            Log.i("elmiraaaa", "DB debug address: $value")
        }
    }
}