package com.farahani.elmira.wordsmagic

import com.farahani.elmira.data.DataModule
import com.farahani.elmira.domain.DomainModule
import com.farahani.elmira.presentation.PresentationModule
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton



@Singleton
@Component(
    modules = [
        AppModule::class,
        DataModule::class,
        PresentationModule::class,
        DomainModule::class
    ]
)
interface AppComponent : AndroidInjector<WordsMagicApp> {

    /**
     *  Builder for this component.
     **/
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<WordsMagicApp>()
}
