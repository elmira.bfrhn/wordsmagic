package com.farahani.elmira.wordsmagic

import android.app.Application
import com.farahani.elmira.presentation.common.di.PerActivity
import com.farahani.elmira.presentation.features.home.view.activity.HomeActivity
import com.farahani.elmira.presentation.features.home.view.activity.HomeActivityModule
import com.farahani.elmira.presentation.features.splash.view.activity.SplashActivity
import com.farahani.elmira.presentation.features.splash.view.activity.SplashActivityModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Module(includes = [AndroidSupportInjectionModule::class])
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun application(app: WordsMagicApp): Application

    @PerActivity
    @ContributesAndroidInjector(modules = [SplashActivityModule::class])
    abstract fun splashActivity(): SplashActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    abstract fun homeActivityInjector(): HomeActivity

}